<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

/** group api routes **/
Route::group(['prefix' => 'api/v1'], function()
{
    Route::resource('authenticate', 'AuthenticateController', [
        'only' => ['index']
    ]);

    Route::post('authenticate/signup', 'AuthenticateController@signUp');
    Route::post('authenticate/signin', 'AuthenticateController@signIn');
    Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');
    Route::post('authenticate/refresh', 'AuthenticateController@refresh');

    Route::get('types', 'TypesController@getTypes');    

    Route::get('recipes/latest', 'RecipesController@getLatestRecipes');    
    Route::get('recipes/{take}/{skip}', 'RecipesController@getRecipes');    
    Route::get('recipes/{id}', 'RecipesController@getRecipe');    

    Route::post('recipes/votes', 'VotesController@create');
    Route::delete('recipes/votes/{id}', 'VotesController@delete');    

});