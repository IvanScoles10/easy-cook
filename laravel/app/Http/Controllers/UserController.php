<?php

namespace App\Http\Controllers;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use Response;

class UserController extends Controller
{
    /**
     * Action to create user
     * @param String first_name
     * @param String last_name
     * @param String email
     * @param String gender
     * @param String password
     * @param String password_confirm
     * @return String token
     */
    public function signup(Request $request) 
    {
        $validator = Validator::make([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'gender' => $request->input('gender'),
            'password' => $request->input('password'),
            'confirm_password' => $request->input('confirm_password') 
        ], [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:4',
            'confirm_password' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            return Redirect::to('/')
                ->withInput()->withErrors($validator);
        } else {
            $credentials = Input::only(
                'first_name', 
                'last_name', 
                'email', 
                'gender',
                'password'
            ); 
            $credentials['password'] = Hash::make($credentials['password']);
            
            try {
                $user = User::create($credentials);
            } catch (Exception $e) {
                return Response::json([
                    'error' => 'User already exists.'
                ], Illuminate\Http\Response::HTTP_CONFLICT);
            }

            $token = \JWTAuth::fromUser($user);
            $request->session()->put('token', Response::json(compact('token')));
            
            return Redirect::to('recipes');
        }
    }
}
