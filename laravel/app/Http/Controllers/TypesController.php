<?php

namespace App\Http\Controllers;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Types;
use App\User;
use Response;

class TypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => []]);
    }

    public function getTypes() {
        $types = array();
        $data = Types::all()->sortBy('description');

        foreach($data as $type) {
            array_push($types, array(
                'id' => $type->id,
                'description' => $type->description
            ));
        };

        return response()->json(compact('types'));
    }
}