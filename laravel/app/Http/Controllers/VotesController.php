<?php

namespace App\Http\Controllers;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Votes;
use Response;

class VotesController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => []]);
    }

    /**
     * Action to create
     * @param String user_id
     * @param String recipe_id
     * @param String like
     */
    public function create(Request $request) 
    {
        $vote = new Votes;

        $vote->user_id = $request->user_id;
        $vote->recipe_id = $request->recipe_id;
        $vote->like = $request->like;

        $vote->save();

        return Response::json(array(
            'id' => $vote->id,
            'like' => $vote->like,
            'user' => array(
                'id' => $vote->user->id,
                'first_name' => $vote->user->first_name,
                'last_name' => $vote->user->last_name,
                'gender' => $vote->user->gender,
                'src_image' => $vote->user->src_image
            )   
        ));
    }

    /**
     * Action to delete 
     * @param String id
     */
    public function delete(Request $request) 
    {
        $response = array();
        $deleted = Votes::where('id', $request->id)->delete();

        $response = ($deleted) ? array('success' => 'deleted') : array('error' => 'error');

        return Response::json($response);
    }
}