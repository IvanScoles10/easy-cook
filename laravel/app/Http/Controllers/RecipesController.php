<?php

namespace App\Http\Controllers;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Recipes;
use App\User;
use Response;

class RecipesController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => []]);
    }

    public function getLatestRecipes() {
        $recipes = array();

        $response = Recipes::take(10)
            ->orderBy('id', 'desc')
            ->skip(0)
            ->get();

        foreach ($response as $recipe) {

            $like = array();
            $dont_like = array();
            
            foreach($recipe->votes as $vote) {
                // LIKE 1, DON'T LIKE 2
                if ($vote->like === '1') {
                    array_push($like, array(
                        'id' => $vote->id,
                        'like' => $vote->like,
                        'user' => array(
                            'id' => $vote->user->id,
                            'first_name' => $vote->user->first_name,
                            'last_name' => $vote->user->last_name,
                            'gender' => $vote->user->gender,
                            'src_image' => $vote->user->src_image
                        )
                    ));
                } else {
                    array_push($dont_like, array(
                        'id' => $vote->id,
                        'like' => $vote->like,
                        'user' => array(
                            'id' => $vote->user->id,
                            'first_name' => $vote->user->first_name,
                            'last_name' => $vote->user->last_name,
                            'gender' => $vote->user->gender,
                            'src_image' => $vote->user->src_image
                        )
                    ));
                }
            } 

            array_push($recipes, array(
                'id' => $recipe->id,
                'title' => $recipe->title,
                'description' => $recipe->description,
                'src_image' => $recipe->src_image,
                'user' => array(
                    'id' => $recipe->user->id,
                    'first_name' => $recipe->user->first_name,
                    'last_name' => $recipe->user->last_name,
                    'gender' => $recipe->user->gender,
                    'src_image' => $recipe->user->src_image
                ),
                'type' => array(
                    'id' => $recipe->type->id,
                    'description' => $recipe->type->description
                ),
                'like' => $like,
                'like_count' => count($like),
                'dont_like' => $dont_like, 
                'dont_like_count' => count($dont_like)
            ));
        
        }

        return Response::json(compact('recipes'));
    }

    public function getRecipes($take, $skip) {
        $recipes = array();

        $response = Recipes::take($take)
            ->orderBy('id', 'desc')
            ->skip($skip)
            ->get();

        foreach ($response as $recipe) {

            $like = array();
            $dont_like = array();

            foreach($recipe->votes as $vote) {
                // LIKE 1, DON'T LIKE 2
                if ($vote->like === '1') {
                    array_push($like, array(
                        'id' => $vote->id,
                        'like' => $vote->like,
                        'user' => array(
                            'id' => $vote->user->id,
                            'first_name' => $vote->user->first_name,
                            'last_name' => $vote->user->last_name,
                            'gender' => $vote->user->gender,
                            'src_image' => $vote->user->src_image
                        )
                    ));
                } else {
                    array_push($dont_like, array(
                        'id' => $vote->id,
                        'like' => $vote->like,
                        'user' => array(
                            'id' => $vote->user->id,
                            'first_name' => $vote->user->first_name,
                            'last_name' => $vote->user->last_name,
                            'gender' => $vote->user->gender,
                            'src_image' => $vote->user->src_image
                        )
                    ));
                }
            } 

            array_push($recipes, array(
                'id' => $recipe->id,
                'title' => $recipe->title,
                'description' => $recipe->description,
                'src_image' => $recipe->src_image,
                'user' => array(
                    'id' => $recipe->user->id,
                    'first_name' => $recipe->user->first_name,
                    'last_name' => $recipe->user->last_name,
                    'gender' => $recipe->user->gender,
                    'src_image' => $recipe->user->src_image
                ),
                'type' => array(
                    'id' => $recipe->type->id,
                    'description' => $recipe->type->description
                ),
                'like' => $like,
                'like_count' => count($like),
                'dont_like' => $dont_like, 
                'dont_like_count' => count($dont_like)
            ));
        }

        return Response::json(compact('recipes'));
    }

    public function getRecipe($id) {
        $recipe = array();

        $response = Recipes::find($id);
    
        $like = array();
        $dont_like = array();

        foreach($response->votes as $vote) {

            // LIKE 1, DON'T LIKE 2
            if ($vote->like === '1') {
                array_push($like, array(
                    'id' => $vote->id,
                    'like' => $vote->like,
                    'user' => array(
                        'id' => $vote->user->id,
                        'first_name' => $vote->user->first_name,
                        'last_name' => $vote->user->last_name,
                        'gender' => $vote->user->gender,
                        'src_image' => $vote->user->src_image
                    )
                ));
            } else {
                array_push($dont_like, array(
                    'id' => $vote->id,
                    'like' => $vote->like,
                    'user' => array(
                        'id' => $vote->user->id,
                        'first_name' => $vote->user->first_name,
                        'last_name' => $vote->user->last_name,
                        'gender' => $vote->user->gender,
                        'src_image' => $vote->user->src_image
                    )
                ));
            }
        } 

        $recipe = array(
            'id' => $response->id,
            'title' => $response->title,
            'description' => $response->description,
            'src_image' => $response->src_image,
            'user' => array(
                'id' => $response->user->id,
                'first_name' => $response->user->first_name,
                'last_name' => $response->user->last_name,
                'gender' => $response->user->gender,
                'src_image' => $response->user->src_image
            ),
            'type' => array(
                'id' => $response->type->id,
                'description' => $response->type->description
            ),
            'like' => $like,
            'like_count' => count($like),
            'dont_like' => $dont_like, 
            'dont_like_count' => count($dont_like)
        );

        return Response::json(compact('recipe'));
    }

}