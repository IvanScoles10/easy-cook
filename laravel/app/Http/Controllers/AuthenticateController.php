<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;

use Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

class AuthenticateController extends Controller 
{

    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => [
            'refresh',
            'signUp',
            'signIn'
        ]]);
    }

    public function index() {
        $users = User::all();
        return $users;
    }

    public function signUp(Request $request) {
        $validator = Validator::make([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'gender' => $request->input('gender'),
            'password' => $request->input('password'),
            'password_confirmation' => $request->input('password_confirmation'),
            'facebook_id' => $request->input('facebook_user_id'),
            'facebook_access_token' => $request->input('facebook_access_token')
        ], [
            'first_name' => 'required|max:45',
            'last_name' => 'required|max:45',
            'email' => 'required|email|unique:users|max:45',
            'gender' => 'required',
            'password' => 'required|max:45',
            'password_confirmation' => 'required|same:password'
        ]);

        if (!$validator->fails()) {
            $credentials = Input::only(
                'first_name', 
                'last_name', 
                'email', 
                'gender',
                'password'
            ); 

            $credentials['password'] = Hash::make($credentials['password']);
            
            try {
                $user = User::create($credentials);
            } catch (Exception $e) {
                return Response::json([
                    'error' => 'User already exists.'
                ], Illuminate\Http\Response::HTTP_CONFLICT);
            }

        
            $token = \JWTAuth::fromUser($user);
        
            return Response::json(compact('token'), 201);
        } else {
            return Response::json(array(
                'error' => $validator->errors()->all()
            ), 200);
        }
    } 

    public function signIn(Request $request) {
        $user = array();
        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $response = User::getUserByEmail($credentials['email']);
        $user = $response->toArray();
        $user['access_token'] = $token;

        return response()->json(
            compact('user')
        );
    }

    public function getAuthenticatedUser()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('user'));
    }

    public function refresh() {
        $token = JWTAuth::getToken();

        if (!$token){
            throw new BadRequestHtttpException('Token not provided');
        }
        try {
            $token = JWTAuth::refresh($token);
        } catch(TokenInvalidException $e){
            throw new AccessDeniedHttpException('The token is invalid');
        }
        return response()->json(['token' => $token]);
    }
}
