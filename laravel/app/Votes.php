<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Votes extends Model
{
    protected $table = 'votes';

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
