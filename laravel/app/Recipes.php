<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipes extends Model
{
    protected $table = 'recipes';

    public function type()
    {
        return $this->belongsTo('App\Types', 'type_id', 'id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function votes()
    {
    	return $this->hasMany('App\Votes', 'recipe_id', 'id');
    }
}
