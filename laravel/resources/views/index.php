<!-- resources/views/index.php -->

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">

        <title>Taste Recipes</title>
      
        <!-- Custom Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

        <!-- Plugin CSS -->
        <link rel="stylesheet" href="css/animate.min.css" type="text/css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/creative.css" type="text/css">

        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/creative.css" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body ng-app="Easycook" scrollspy ng-class="{ 'body-authenticated': authenticated }">
        <!-- navbar view !authenticated -->
        <nav affix class="navbar navbar-default navbar-fixed-top" ng-show="!authenticated">
            <div class="container-fluid">
                <div ng-include src="'views/navbar.html'"></div>
            </div>
        </nav>

        <!-- navbar view !authenticated -->
        <!--
        <nav affix class="navbar navbar-default navbar-fixed-top navbar-authenticated" ng-show="authenticated">
            <div class="container-fluid">
                <div ng-include src="'views/navbar-authenticated.html'"></div>
            </div>
        </nav>
        -->

        <!-- header view !authenticated -->
        <header ng-include src="'views/header.html'" ng-show="!authenticated"></header>

        <!-- content authenticated -->
        <div ng-show="authenticated">
            <div style="margin-top: 50px;" ui-view></div>
        </div>
        
        <!-- about view !authenticated -->
        <!--
        <section ng-include src="'views/connect.html'" class="bg-primary" id="about" ng-show="!authenticated"></section>
        -->

        <!-- latest recipes view !authenticated -->
        <!--
        <div ng-include src="'views/latest-recipes.html'" class="no-padding latest-recipes" id="recipes" ng-show="!authenticated"></div>

        <!-- register view !authenticated -->
        <!--
        <section ng-include src="'views/register.html'" class="bg-primary register" id="register" ng-show="!authenticated"></section> 
        -->  

    </body>

    <!-- Application Dependencies -->
    <script src="node_modules/angular/angular.js"></script>
    <script src="node_modules/angular-animate/angular-animate.min.js"></script>
    <script src="node_modules/angular-resource/angular-resource.js"></script>
    <script src="node_modules/angular-ui-router/build/angular-ui-router.js"></script>
    <script src="node_modules/satellizer/satellizer.js"></script>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/fittext.js/jquery.fittext.js"></script>
    <script src="node_modules/angular-local-storage/dist/angular-local-storage.min.js"></script>
    <script src="node_modules/ng-file-upload/dist/ng-file-upload-shim.min.js"></script> <!-- for no html5 browsers support -->
    <script src="node_modules/ng-file-upload/dist/ng-file-upload.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ui-bootstrap-tpls.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/wow.min.js"></script>

    <!-- Application Scripts -->
    <script src="scripts/app.js"></script>
    <script src="scripts/util.directive.js"></script>
    <!-- Resources -->
    <script src="scripts/resources/recipes.factory.js"></script>
    <!-- Services -->
    <script src="scripts/resources/recipes.service.js"></script>
    <!-- Controllers -->
    <script src="scripts/controllers/main.controller.js"></script>
    <script src="scripts/controllers/home.controller.js"></script>
    <script src="scripts/controllers/dashboard.controller.js"></script>
    <script src="scripts/controllers/recipe.controller.js"></script>
</html>