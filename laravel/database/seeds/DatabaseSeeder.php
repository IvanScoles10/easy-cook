<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('users')->insert([
            'id' => 1,
            'first_name' => 'Néstor Iván',
            'last_name' => 'Scoles',
            'email' => 'nivan.scoles@hotmail.com',
            'gender' => 'male',
            'password' => Hash::make('1234'),
            'src_image' => 'http://taste-recipes.com/img/user/01.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
            'id' => 2,
            'first_name' => 'Sofia',
            'last_name' => 'Carante',
            'email' => 'soficarante@facebook.com',
            'gender' => 'female',
            'password' => Hash::make('1234'),
            'src_image' => 'http://taste-recipes.com/img/user/02.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
            'id' => 3,
            'first_name' => 'Ana',
            'last_name' => 'Flores',
            'email' => 'anaflores@hotmail.com',
            'gender' => 'female',
            'password' => Hash::make('1234'),
            'src_image' => 'http://taste-recipes.com/img/user/03.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('types')->insert([
            'id' => 1,
            'description' => 'Pizzas'
        ]);

        DB::table('types')->insert([
            'id' => 2,
            'description' => 'Pastas'
        ]);

        DB::table('recipes')->insert([
            'id' => 1,
            'user_id' => 1,
            'type_id' => 1,
            'title' => 'Hamburpizza',
            'description' => 'Para la hamburguesa, en un bol, colocar la carne picada, sumar la cebolla, ajo y morrón picados, el huevo crudo y condimentar. Comenzar a integrar y amasar por unos 10 minutos.',
            'src_image' => 'http://taste-recipes.com/img/category/pizzas/pizza.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('recipes')->insert([
            'id' => 2,
            'user_id' => 2,
            'type_id' => 1,
            'title' => 'Pizza de rúcula',
            'description' => 'Para la hamburguesa, en un bol, colocar la carne picada, sumar la cebolla, ajo y morrón picados, el huevo crudo y condimentar. Comenzar a integrar y amasar por unos 10 minutos.',
            'src_image' => 'http://taste-recipes.com/img/category/pizzas/pizza.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('recipes')->insert([
            'id' => 3,
            'user_id' => 3,
            'type_id' => 1,
            'title' => 'Pizza de mandioca',
            'description' => 'Para la hamburguesa, en un bol, colocar la carne picada, sumar la cebolla, ajo y morrón picados, el huevo crudo y condimentar. Comenzar a integrar y amasar por unos 10 minutos.',
            'src_image' => 'http://taste-recipes.com/img/category/pizzas/pizza.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('recipes')->insert([
            'id' => 4,
            'user_id' => 1,
            'type_id' => 1,
            'title' => 'Pizza integral crocante con vegetales y salchicha parrillera',
            'description' => 'Para la hamburguesa, en un bol, colocar la carne picada, sumar la cebolla, ajo y morrón picados, el huevo crudo y condimentar. Comenzar a integrar y amasar por unos 10 minutos.',
            'src_image' => 'http://taste-recipes.com/img/category/pizzas/pizza.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('recipes')->insert([
            'id' => 5,
            'user_id' => 1,
            'type_id' => 1,
            'title' => 'Pan lavash',
            'description' => 'Para la hamburguesa, en un bol, colocar la carne picada, sumar la cebolla, ajo y morrón picados, el huevo crudo y condimentar. Comenzar a integrar y amasar por unos 10 minutos.',
            'src_image' => 'http://taste-recipes.com/img/category/pizzas/pizza.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('recipes')->insert([
            'id' => 6,
            'user_id' => 1,
            'type_id' => 1,
            'title' => 'Focaccia de papa y romero rellena de queso',
            'description' => 'Para la hamburguesa, en un bol, colocar la carne picada, sumar la cebolla, ajo y morrón picados, el huevo crudo y condimentar. Comenzar a integrar y amasar por unos 10 minutos.',
            'src_image' => 'http://taste-recipes.com/img/category/pizzas/pizza.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('recipes')->insert([
            'id' => 7,
            'user_id' => 1,
            'type_id' => 2,
            'title' => 'Fetuccinis Boloñesa',
            'description' => 'Para la hamburguesa, en un bol, colocar la carne picada, sumar la cebolla, ajo y morrón picados, el huevo crudo y condimentar. Comenzar a integrar y amasar por unos 10 minutos.',
            'src_image' => 'http://taste-recipes.com/img/category/pizzas/pizza.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('recipes')->insert([
            'id' => 8,
            'user_id' => 1,
            'type_id' => 2,
            'title' => 'Calzones a la parrilla de varios rellenos',
            'description' => 'Un plato de pasta clásico que a todos gusta y que no pasa de moda y que, además es fácil, rápido y económico.',
            'src_image' => 'http://taste-recipes.com/img/category/pizzas/pizza.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('recipes')->insert([
            'id' => 9,
            'user_id' => 2,
            'type_id' => 2,
            'title' => 'Macarrones con queso bajos en calorias',
            'description' => 'Son unos macarrones muy sencillos, sin complicación de ingredientes. Perfectos para niños. Puedes hacerlos con o sin gratinar, yo lo hicé gratinados aunque reconozco que gratinar con queso light siempre hace que te quede peor que si fuera con queso normal.',
            'src_image' => 'http://taste-recipes.com/img/category/pizzas/pizza.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        // like 1 - don't like 2
        DB::table('votes')->insert([
            'id' => 1,
            'recipe_id' => 1,
            'user_id' => 1,
            'like' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('votes')->insert([
            'id' => 2,
            'recipe_id' => 2,
            'user_id' => 1,
            'like' => 2,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        Model::reguard();
    }
}
