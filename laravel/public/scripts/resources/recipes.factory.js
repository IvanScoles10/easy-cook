angular.module('Easycook').factory('recipesFactory', function($resource) {
    var endpoint = 'api';
    return {
        getLatestRecipes: $resource(endpoint + '/recipes/latest', {}, {
            query: {
                method: 'GET',
                isArray: true,
                interceptor: {
                    'response': function(response) {
                        return response;
                    }
                }
            }
        }),
        getRecipes: $resource(endpoint + '/recipes/:take/:skip', {
            take: '@take',
            skip: '@skip'
        }, {
            query: {
                method: 'GET',
                isArray: true,
                interceptor: {
                    'response': function(response) {
                        return response;
                    }
                }
            }
        })
    }
});