angular.module('Easycook').service('recipesService', function($q, recipesFactory) {

	var self = this;

	this.getLatestRecipes = function() {
  		var deferred = $q.defer();
		
		recipesFactory.getLatestRecipes.query(function(response) {
			deferred.resolve(response);
		}, function(error) {
			deferred.reject(error);
		});

  		return deferred.promise;
	};

	this.getRecipes = function(take, skip) {
  		var deferred = $q.defer();
		
		recipesFactory.getRecipes.query({
			take: take,
			skip: skip
		}, {}, function(response) {
			deferred.resolve(response);
		}, function(error) {
			deferred.reject(error);
		});

  		return deferred.promise;
	};

});
