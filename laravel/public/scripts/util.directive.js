angular.module('Easycook')
    .directive('fittext', function() {
        return {
            restrict: 'AE',
            scope: {
                ngMinFontSize: '@',
                ngMaxFontSize: '@'
            },
            link: function(scope, element, attrs) {
                $(element).fitText(
                    1.2, {
                        minFontSize: scope.ngMinFontSize,
                        maxFontSize: scope.ngMaxFontSize
                    }
                );
            }
        };
    })
    .directive('affix', function() {
        return {
            restrict: 'AE',
            link: function(scope, element, attrs) {
                $(element).affix({
                    offset: {
                        top: 100
                    }
                });
            }
        };
    })
    .directive('scrollspy', function() {
        return {
            restrict: 'AE',
            link: function(scope, element, attrs) {
                $(element).scrollspy({
                    target: '.navbar-fixed-top',
                    offset: 51
                });
            }
        };
    })
    .directive('pagescroll', function() {
        return {
            restrict: 'AE',
            link: function(scope, element, attrs) {
                $(element).bind('click', function(event) {
                    var $anchor = $(this);
                    $('html, body').stop().animate({
                        scrollTop: ($('#' + attrs.pointer).offset().top - 50)
                    }, 1250, 'easeInOutExpo');
                    event.preventDefault();
                });
            }
        };
    })
    .directive('match', function() {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, model) {
                if (!attrs.match) {
                    console.error('match expects a model as an argument!');
                    return;
                }
                scope.$watch(attrs.match, function (value) {
                    if (model.$viewValue !== undefined && model.$viewValue !== '') {
                        model.$setValidity('match', value === model.$viewValue);
                    }
                });
                model.$parsers.push(function (value) {
                    if (value === undefined || value === '') {
                        model.$setValidity('match', true);
                        return value;
                    }
                    var isValid = value === scope.$eval(attrs.match);
                    model.$setValidity('match', isValid);
                    return isValid ? value : undefined;
                });
            }
        };
    });