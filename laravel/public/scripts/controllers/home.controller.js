angular.module('Easycook')
    .controller('HomeCtrl', function ($rootScope, $scope, recipesService) {

        $scope.error = {};
        $scope.loading = {};

        $scope.loading.latestRecipes = true;
        $scope.latestRecipes = [];

        $scope.getLatestRecipes = function() {
            recipesService.getLatestRecipes().then(function(response) {
                $scope.loading.latestRecipes = true;
                if (response.status === 200) {
                    $scope.latestRecipes = response.data;
                }
            }, function(error) {
                console.log(error);
            });
        };

    });