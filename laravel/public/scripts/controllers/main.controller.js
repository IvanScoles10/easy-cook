angular.module('Easycook')
    .controller('MainCtrl', function ($rootScope, $scope, $state, $http, $auth, $uibModal, $location, localStorageService) {

        $scope.signin = {
            email: null,
            password: null,
            error: false
        };

        $scope.singup = {
            first_name: null,
            last_name: null,
            email: null,
            password: null,
            password_confirm: null,
            gender: 'male',
            error: false
        };

        $rootScope.user = null;

        /** signin user **/
        $scope.signIn = function (form) {
            if (form.$valid) {
                var credentials = {
                    email: $scope.signin.email,
                    password: $scope.signin.password
                };

                $auth.login(credentials).then(function() {
                    return $http.get('api/authenticate/user').then(function(response) {
                        /** get user **/
                        var user = JSON.stringify(response.data.user);

                        /** authenticate to true **/
                        $rootScope.authenticated = true;
                        $rootScope.user = response.data.user;

                        /** set user in local storage **/
                        localStorageService.set('user', response.data.user);

                        /** go to dashboard **/
                        $state.go('dashboard');
                    });
                }, function(error) {
                    /** invalid credentials **/
                    $scope.signin.error = true;
                });
            }
        };

        /** sign out **/
        $scope.signOut = function() {
            $auth.logout().then(function() {
                /** authenticate to true **/
                $rootScope.authenticated = false;
                $rootScope.user = {};

                /** set user in local storage **/
                localStorageService.clearAll()

                /** go to dashboard **/
                $state.go('home');
            });
        };

        /** change gender **/
        $scope.changeGender = function(gender) {
            $scope.signup.gender = gender;
        };

        /** signup user **/
        $scope.signUp = function(form) {
            if (form.$valid) {
                var data = {
                    first_name: $scope.signup.first_name,
                    last_name: $scope.signup.last_name,
                    email: $scope.signup.email,
                    password: $scope.signup.password,
                    password_confirm: $scope.signup.password_confirm,
                    gender: $scope.signup.gender
                };

                return $http.post('api/authenticate/signup', data).then(function(response) {
                    if (response.status === 201) {

                        $auth.setToken(response.data.token);

                        $auth.login(data).then(function() {
                            return $http.get('api/authenticate/user').then(function(response) {
                                var user = JSON.stringify(response.data.user);

                                $rootScope.user = response.data.user;

                                localStorageService.set('user', response.data.user);
                                
                                $state.go('dashboard');
                            });
                        }, function(error) {
                            console.log(error);
                        });
                    } else {
                        $scope.signup.error = 'The email address is already in use.'
                    }
                }, function(error) {
                    /** invalid credentials **/
                    $scope.signin.error = true;
                });
            }
        };

        /** logout user **/
        $scope.logout = function() {
            $auth.logout().then(function() {
                /** delete user in local storage data **/
                localStorage.removeItem('user');
                /** authenticated to false **/
                $rootScope.authenticated = false;
                /** remove the current user info from rootscope **/
                $rootScope.user = null;
            });
        };

        $scope.openRecipe = function(){ 
            $uibModal.open({
                animation: true,
                templateUrl: 'views/recipe.html',
                controller: 'RecipeCtrl',
                size: 'lg',
                resolve: {}
            });

        };
    });