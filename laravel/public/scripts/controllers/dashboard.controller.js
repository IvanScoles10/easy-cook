angular.module('Easycook')
    .controller('DashboardCtrl', function ($rootScope, $scope, $state, localStorageService, recipesService) {

        $scope.user = localStorageService.get('user');
        $scope.error = {};
        $scope.loading = {};

        $scope.loading.recipes = true;
        $scope.recipes = [];

        $scope.getRecipes = function(take, skip) {
            recipesService.getRecipes(take, skip).then(function(response) {
                $scope.loading.getRecipes = true;
                if (response.status === 200) {
                    $scope.filter.skip = $scope.filter.skip + 6;
                    $scope.filter.take = $scope.filter.take;

                    $scope.setRecipes(response.data);
                }
            }, function(error) {
                console.log(error);
            });
        };

        $scope.setRecipes = function(data) {
            angular.forEach(data, function(item, value) {
                $scope.recipes.push(item);
            });
        };

        $scope.filter = {};
        $scope.filter.take = 6;
        $scope.filter.skip = 0;

        $scope.moreRecipes = function() {
            $scope.getRecipes($scope.filter.take, $scope.filter.skip);
        };

    });