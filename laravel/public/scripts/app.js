// public/scripts/app.js

(function() {
    'use strict';
    angular
        .module('Easycook', ['ngAnimate', 'ngResource', 'ngFileUpload', 'LocalStorageModule', 'ui.bootstrap', 'ui.router', 'satellizer'])
        .config(function($stateProvider, $urlRouterProvider, $authProvider, $httpProvider, $provide, localStorageServiceProvider) {
          
            /** set prefix to easycook project local storage data **/
            localStorageServiceProvider
                .setPrefix('Easycook');

            /** 
             * Satellizer configuration that specifies which API
             * route the JWT should be retrieved from
             **/
            $authProvider.loginUrl = '/api/authenticate';

            /** 
             * Redirect to the auth state if any other states
             * are requested other than users
             **/

            $urlRouterProvider
                .otherwise('/home');
            
            $stateProvider
                .state('app', {
                    abstract: true,
                    url: '/home'
                })
                .state('dashboard', {
                    url: '/dashboard',
                    templateUrl: '/views/dashboard_view.html',
                    controller: 'DashboardCtrl'
                });
                
            $httpProvider.interceptors.push(['$rootScope', '$location', '$q', '$injector', function (rootScope, location, q, $injector) {
                return {
                    'request': function (config) {
                        var auth = $injector.get('$auth');

                        config.headers = config.headers || {};

                        if (auth.isAuthenticated()) {
                            rootScope.authenticated = true;

                            var token = auth.getToken();

                            config.headers.Authorization = 'Bearer ' + token;
                            var path = location.$$path;
                            if (path === '/home' || path === '') {
                                location.path('/dashboard');
                            }
                        } else {
                            rootScope.authenticated = false;

                            if (location.$$path !== '/home') {
                                location.path('/home');
                            }
                        }

                        return config;
                    }
                };
            }]);

        });

    })();